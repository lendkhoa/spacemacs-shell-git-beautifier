'''
Statistic class processes statistical data of
each command
'''
import datetime
import errno
import xml.etree.ElementTree as ET
import os
import re
import sys
import subprocess
import platform
import time
from itertools import islice

def main():
    file_dir = "./db.txt"
    number_of_calls, total_time = load_from_db(file_dir)

    start_time = time.time()
    subprocess.check_output(["git", "status"])
    end_time = time.time()

    increment_call_count(number_of_calls, file_dir)
    update_total_time(end_time - start_time, total_time, file_dir)
    update_avg_time(end_time - start_time, file_dir)

def load_from_db(file_dir):
    number_of_calls = 0
    total_time = 0
    try:
        file = open(file_dir, "r")
        for line in file:
            if ">" in line:
                if "number_of_calls" in line:
                    number_of_calls = int(line[line.index(':') + 1:])
                if "total_time" in line:
                    total_time = float(line[line.index(':') + 1:])
        return number_of_calls, total_time

    except FileNotFoundError:
        print("Error: cannot file " + file_dir)

def update_avg_time(time, file):
    try:
        lines = []
        with open (file, "rt") as fin:
            lines = fin.readlines()
        # update time
        for i, val in enumerate(lines):
            cur_line = lines[i]
            if "avg_time:" in cur_line:
                lines[i] = "> avg_time: " + str(time)

        with open (file, "wt") as fout:
            fout.write(''.join(lines))
    except FileNotFoundError:
        print("Error: cannot file " + file)

def increment_call_count(number_of_calls, file):
    try:
        lines = []
        with open (file, "rt") as fin:
            lines = fin.readlines()
        # update time
        for i, val in enumerate(lines):
            cur_line = lines[i]
            if "number_of_calls:" in cur_line:
                number_of_calls += 1
                lines[i] = "> number_of_calls: " + str(number_of_calls)

        with open (file, "wt") as fout:
            fout.write(''.join(lines))
    except FileNotFoundError:
        print("Error: cannot file " + file)

def update_total_time(new_time, total_time, file):
    try:
        lines = []
        with open (file, "rt") as fin:
            lines = fin.readlines()
        # update time
        for i, val in enumerate(lines):
            cur_line = lines[i]
            if "total_time:" in cur_line:
                total_time += new_time
                lines[i] = "> total_time: " + str(total_time)

        with open (file, "wt") as fout:
            fout.write(''.join(lines))
    except FileNotFoundError:
        print("Error: cannot file " + file)


def get_avg_time(number_of_calls, total_time):
    avg_time = 0
    if number_of_calls > 0:
        avg_time = total_time / number_of_calls
    return avg_time;

if __name__ == '__main__':
    main()
