import errno
import xml.etree.ElementTree as ET
import os
import GitPrinter as gitprinter
import platform
import re
import sys
import subprocess
import webbrowser
from itertools import islice

"""
Spacemacs shell git beautifier
Customize git output on Spacemacs shell

Author: Khoa Le
Version: Oct 19, 2018
"""

def main():
    git_command, commit = gather_params()
    executer(git_command, commit)

def executer(git_command, commit):
    optional_file = ""
    version = sys.version[0:1]
    if "gl" in git_command or "GL" in git_command:
        try:
            num_commits = commit
            if int(num_commits) > 0:
                output = git_log(num_commits)
                gitprinter.print_command_output(output, "BOLD")
                commits = extract_commits_sha(output)
                user_input = wait_for_commits_selection(commits)

                # if the user want to checkout specific commit
                if "co" in user_input:
                    git_checkout(commits[int(user_input[user_input.index("co") + 2:])])
                    exit()
                # if the user select commits SHA
                if len(user_input) == 1:
                    sfc(commits[int(user_input[0])].strip(), "")
                else:
                    sfc(commits[int(user_input[0])].strip(), commits[int(user_input[1])].strip())
            else:
                gitprinter.cprint("Error: Enter a non zero number of commits to show git log", gitprinter.bcolors.FAIL)
        except ValueError:
            gitprinter.cprint("Error: Enter number of commits to show git log", gitprinter.bcolors.FAIL)
        except TypeError:
            gitprinter.cprint("Unknown command - Exit program", gitprinter.bcolors.FAIL)

    elif "gitStatus" in git_command:
        output = git_status()
        files = extract_files(output.splitlines(), "gitStatus")
        if len(files) > 0:
            try:
                file_selected = wait_for_file_selected(files)
                git_diff(file_selected)
            except TypeError:
                gitprinter.cprint("Error: Invalid index or option selected", gitprinter.bcolors.FAIL)
        else:
            gitprinter.print_command_output(output, "")
    # git branch local
    elif re.match("gb\\b", git_command):
        try:
            output = subprocess.check_output(["git", "branch"])
            branches = extract_branches(output)
            if len(branches) > 0:
                gitprinter.print_git_branch(branches)
                branch = wait_for_branch_selected(branches)
                subprocess.check_output(["git", "checkout", branch])
            else:
                gitprinter.print_command_output(output, "BOLD")
        except subprocess.CalledProcessError as e:
                gitprinter.cprint("Error: Verify this is a git repo", gitprinter.bcolors.FAIL)
    # git branch remote
    elif re.match("gba\\b", git_command):
        try:
            output = subprocess.check_output(["git", "branch", "-a"])
            branches = extract_branches(output)
            if len(branches) > 1:
                gitprinter.print_git_branch(branches)
                branch = wait_for_branch_selected(branches)
                subprocess.check_output(["git", "checkout", branch])
            else:
                gitprinter.print_command_output(output, "BOLD")
        except subprocess.CalledProcessError as e:
                gitprinter.cprint("Error: Verify this is a git repo", gitprinter.bcolors.FAIL)
    # git show file change
    elif "sfc" in git_command:
        sfc(commit, "")
    elif "showChange" in git_command:
        if len(sys.argv) == 4:
            optional_file = sys.argv[3]
        git_show_change(commit, optional_file)
    elif "gitDiff" in git_command:
        if len(sys.argv) == 3:
            optional_file = sys.argv[2]
        git_diff(optional_file)
    elif "gitLogFile" in git_command:
        num_commits = commit
        file_target = ""
        if len(sys.argv) == 4:
            file_target = sys.argv[3]
            git_log_file(num_commits, file_target)
    # open git origin with browser
    elif "showOrigin" or "openOrigin" in git_command:
        origin = subprocess.check_output(["git", "remote", "-v"]).decode("utf-8")
        branch = subprocess.check_output(["git", "status"]).decode("utf-8").split("On branch ")[1].splitlines()[0]
        the_url = origin.splitlines()[1].strip()
        remote = "https://" + the_url.splitlines()[0].split("origin")[1].split(" (push)")[0].split("@")[1].replace(".com:", ".com/")
        remote = remote.replace(".git", "/tree/" + branch)
        print(f'\n {remote} \n')
        webbrowser.open(remote + "", new=2)
    elif "help" in git_command:
        gitprinter.print_help()
    else:
        print("Command or alias is not defined")
    gitprinter.print_separation_line()

"""
Git checkout
@commit: the commit SHA
"""
def git_checkout(commit):
    output = subprocess.check_output(["git", "checkout", commit.strip()])
    gitprinter.print_command_output(output, "BOLD")

"""
show file change
@commit: the main commit SHA to be traverse by the command
@optional_commit: the optional commit SHA
"""
def sfc(commit, optional_commit):
    output = git_show_file_change(commit, optional_commit, False)
    try:
        files = extract_files(output.splitlines(), "sfc")
        if len(files) > 0:
            try:
                while (True):
                    gitprinter.print_git_show_file_change(output)
                    user_input = wait_for_file_selected(files)
                    git_show_change(commit, optional_commit, user_input)
            except TypeError:
                print(TypeError)
                gitprinter.cprint("Error: Invalid index or option selected", gitprinter.bcolors.FAIL)

    except AttributeError:
        gitprinter.cprint("Error: showChange command requires commit SHA or file directory is not correct (relative to git root folder).\nTry again in the parent directory", gitprinter.bcolors.FAIL)

"""
git diff
@optional_file: the optional commit SHA, if not provided then
it will compare to the previous commit
"""
def git_diff(optional_file):
    if len(optional_file) == 0:
        output = subprocess.check_output(["git", "diff"])
        gitprinter.print_code_changed(output)
    else:
        output = subprocess.check_output(["git", "diff", optional_file])
        gitprinter.print_code_changed(output)

"""
Shows change between two commits of a specific file
@commit: the main commit SHA
@optional_commit: the optional commit SHA to compare to
@user_input: user input, can be file directory or defined command
"""
def git_show_change(commit, optional_commit, user_input):
    try:
        gitprinter.print_pair_info(commit, optional_commit)
        output = ""

        if len(optional_commit) == 0 and len(user_input) == 0:
            output = subprocess.check_output(["git", "diff", str(commit)+"^1", commit])
        elif len(optional_commit) == 0 and len(user_input) > 0:
            output = subprocess.check_output(["git", "diff", str(commit)+"^1", commit, user_input])
        elif len(optional_commit) > 0 and len(user_input) > 0:
            output = subprocess.check_output(["git", "diff", optional_commit, commit, user_input])
        else:
            output = subprocess.check_output(["git", "diff", optional_commit, commit])
        gitprinter.print_code_changed(output)

    except subprocess.CalledProcessError:
        gitprinter.cprint("Error: showChange command requires commit SHA or file directory is not correct (relative to git root folder).\nTry again in the parent directory", gitprinter.bcolors.FAIL)

"""
Shows commit graph
@commit: the number of commits
"""
def git_log(nums_commits):
    try:
        if int(nums_commits) + 1 > 0:
            output = subprocess.check_output(["git", "log", "-"+str(nums_commits), "--graph", "--pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'", "--abbrev-commit"])
            return output;
        else:
            gitprinter.cprint("Number of commits should be greater than zero", gitprinter.bcolors.FAIL)
            quit()
    except AttributeError:
        gitprinter.cprint("Error: git log command requires commit number", gitprinter.bcolors.FAIL)
    except ValueError:
        gitprinter.cprint("Error: git log command requires commit number", gitprinter.bcolors.FAIL)

def git_pull():
    output = subprocess.check_output(["git", "pull"])
    gitprinter.print_command_output(output, "WARNING")

"""
Show files changed between commits
@commit: the main commit SHA
@optional_commit: the optional commit SHA to compare to
"""
def git_show_file_change(commit, optional_commit, inform):
    try:
        if len(optional_commit) == 0:
            output = subprocess.check_output(["git", "diff-tree", "--no-commit-id", "--name-status", "-r", commit])
        else:
            output = subprocess.check_output(["git", "diff-tree", "--no-commit-id", "--name-status", "-r", optional_commit, commit])
        if inform:
            print("File(s) changed:")
            gitprinter.print_git_show_file_change(output)
        return output;
    except subprocess.CalledProcessError:
        gitprinter.cprint("Error: git diff-tree requires commit SHA", gitprinter.bcolors.FAIL)

def git_status():
    try:
        output = subprocess.check_output(["git", "status"])
        gitprinter.print_git_status_files(output)
        return output
    except subprocess.CalledProcessError:
        exit()

"""
Show commits that change a specific file
@num_commits: the number of commits modified file
@file_target: the file to log
"""
def git_log_file(num_commits, file_target):
    commits = subprocess.check_output(["git", "log", "-" + num_commits, file_target])
    commits = commits.splitlines()
    commits_modified_file = []
    for idx in range(0, len(commits) - 1):
        current = commits[idx].decode("utf-8").strip()
        next = commits[idx + 1].decode("utf-8").strip()
        if current.startswith("commit ") and next.startswith("Author"):
            commits_modified_file.append(current[current.index("commit ") + 7 :].strip())

    if len(commits_modified_file) > 0:
        idx = 0
        commit_info_cache = []
        for commit in commits_modified_file:
            commit_info = subprocess.check_output(["git", "show", "--format=%h - %an, %ar : %s", "--no-patch", commit]).splitlines()
            print("[" + str(idx) + "] " + commit_info[0].decode("utf-8"))
            idx += 1
            commit_info_cache.append(commit_info[0].decode("utf-8"))

        # ask user which commit to view changes
        commit_selected = ""
        version = sys.version[0:1]
        gitprinter.cprint("Select commit to view changes, Ctrl + c or 'q' to quit", gitprinter.bcolors.WARNING)
        stop = False
        while (not stop):
            if int(version) > 2:
                commit_selected = input()
            else:
                commit_selected = raw_input()

            if "q" in commit_selected:
                stop = True
                quit()
            else:
                print("Selected " + str(commits_modified_file[int(commit_selected)]))
                git_show_change(commits_modified_file[int(commit_selected)], "", file_target)
                gitprinter.print_separation_line()
                gitprinter.cprint("Select commit to view changes, Ctrl + c or 'q' to quit", gitprinter.bcolors.WARNING)
                for idx, val in enumerate(commit_info_cache):
                    print("[" + str(idx) + "] " + commit_info_cache[idx])
    else:
        gitprinter.cprint("No commit in the last " + str(num_commits) + " commits modified file " + file_target, gitprinter.bcolors.WARNING)

def wait_for_file_selected(files):
    print("___")
    gitprinter.cprint("\n Select file index to show diff, Ctr-c-c or 'q' to exit", gitprinter.bcolors.WARNING)
    try:
        file_selected = ""
        version = sys.version[0:1]
        if int(version) > 2:
            file_selected = input()
        else:
            file_selected = raw_input()

        if file_selected == "q":
            exit()
        elif file_selected == "a":
            return "";
        gitprinter.cprint("Selected: " + files[int(file_selected) - 1], gitprinter.bcolors.WARNING)
        return files[int(file_selected) - 1].strip()
    except TypeError:
        gitprinter.cprint("Error: ", gitprinter.bcolors.FAIL)
    except ValueError:
        gitprinter.cprint("Error: File index is not numeric", gitprinter.bcolors.FAIL)
    except IndexError:
        gitprinter.cprint("Error: File index out of bound", gitprinter.bcolors.FAIL)
    except KeyboardInterrupt:
        gitprinter.cprint("Exit", gitprinter.bcolors.FAIL)

def wait_for_branch_selected(branches):
    gitprinter.cprint("\n Select branch to checkout, Ctr-c-c or 'q' to exit", gitprinter.bcolors.WARNING)
    try:
        b_selected = ""
        version = sys.version[0:1]
        if int(version) > 2:
            b_selected = input()
        else:
            b_selected = raw_input()

        if b_selected == "q":
            exit()
        elif b_selected == "a":
            return "";
        return branches[int(b_selected) - 1].strip()
    except TypeError:
        gitprinter.cprint("Error: ", gitprinter.bcolors.FAIL)
    except ValueError:
        gitprinter.cprint("Error: File index is not numeric", gitprinter.bcolors.FAIL)
    except IndexError:
        gitprinter.cprint("Error: File index out of bound", gitprinter.bcolors.FAIL)
    except KeyboardInterrupt:
        gitprinter.cprint("Exit", gitprinter.bcolors.FAIL)

def wait_for_commits_selection(commits):
    print("___")
    gitprinter.cprint("\n Select commits index (0-2) to show diff", gitprinter.bcolors.WARNING)
    gitprinter.cprint(" command: co <index> to checkout detached HEAD, Ctr-c-c or 'q' to exit", gitprinter.bcolors.WARNING)
    try:
        user_input = ""
        version = sys.version[0:1]
        # python 2 or 3
        if int(version) > 2:
            user_input = input()
        else:
            user_input = raw_input()

        if user_input == "q":
            exit()

        # validate input format
        validated = validate_selected_commit_input(user_input, commits)

        if validated:
            if "co" in user_input:
                return user_input
            else:
                user_input_commit = []
                if "-" in user_input:
                    user_input_commit.append(user_input[0:user_input.index("-")])
                    user_input_commit.append(user_input[user_input.index("-") + 1:])
                else:
                    user_input_commit.append(user_input)
                return user_input_commit
    except TypeError:
        gitprinter.cprint("Error: ", gitprinter.bcolors.FAIL)
        exit()
    except ValueError:
        gitprinter.cprint("Error: File index is not numeric", gitprinter.bcolors.FAIL)
        exit()
    except IndexError:
        gitprinter.cprint("Error: File index out of bound", gitprinter.bcolors.FAIL)
        exit()
    except KeyboardInterrupt:
        gitprinter.cprint("Exit", gitprinter.bcolors.FAIL)
        exit()

"""
Validates user input for the follow up action
"""
def validate_selected_commit_input(user_input, commits):
    if len(user_input) < 1:
        return False
    else:
        # check if user input is a valid checkout commit command
        if "co" in user_input and user_input.index("co") == 0 and int(user_input[user_input.index("co") + 2 :]):
            return True;

        if "-" not in user_input:
            if not user_input.isdigit() or len(commits) < int(user_input):
                gitprinter.cprint("Error: Format error, index entered is not numeric characters or out of bound", gitprinter.bcolors.FAIL)
                return False
        else:
            first_index = user_input[0:user_input.index("-")]
            second_index = user_input[user_input.index("-") + 1:]

            if not first_index.isdigit() or not second_index.isdigit():
                gitprinter.cprint("Error: Format error, user_input entered are not numeric characters or out of bound", gitprinter.bcolors.FAIL)
                return False
            elif int(first_index) > len(commits) or int(second_index) > len(commits):
                gitprinter.cprint("Error: User_Input entered are out of bound", gitprinter.bcolors.FAIL)
                return False
    return True

def extract_commits_sha(output):
    commits = []
    lines = output.splitlines()
    for line in enumerate(lines):
        cur_line = line[1].decode("utf-8")
        if "ago" in cur_line:
            commits.append(cur_line[cur_line.index("'") + 1:cur_line.index("-")])
    return commits

def extract_files(output, command):
    files = []
    for i, line in enumerate(output):
        cur_line = line.decode("utf-8")
        if command == "gitStatus":
            if "modified" in cur_line:
                file_dir = cur_line[cur_line.index(":   ") + 4 :]
                files.append(file_dir)
            elif "new file" in cur_line:
                file_dir = cur_line[cur_line.index(":   ") + 4 :]
                files.append(file_dir)
            elif "Untracked files:" in cur_line:
                j = i + 3
                file_dir = re.sub(r"[\n\t\s]*", "", output[j].decode("utf-8"))
                while len(file_dir) > 0:
                    files.append(file_dir)
                    j+=1
                    file_dir = re.sub(r"[\n\t\s]*", "", output[j].decode("utf-8"))
        elif command == "sfc":
            if "A" == cur_line[0] or "M" == cur_line[0]:
                file_dir = cur_line[1:]
                files.append(file_dir)

    return files

def extract_branches(output):
    lines = output.splitlines()
    branches = []
    b_set = set()
    for line in lines:
        cur_line = line.decode("utf-8").strip()
        if "origin" in cur_line:
            cur_line = cur_line[cur_line.rfind("origin") + 7:]
        if "*" in cur_line:
            continue
        b_set.add(cur_line)
    for b in sorted_nicely(b_set):
      branches.append(b)
    return branches

def gather_params():
    filtered_params = [arg for arg in sys.argv if arg !=""]
    if len(sys.argv) == 2:
        return (sys.argv[1], "")
    if len(sys.argv) >= 3:
        return (filtered_params[1], int(filtered_params[2]))
    return ("./", "")

'''
source: https://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
'''
def sorted_nicely( l ): 
    """ Sort the given iterable in the way that humans expect.""" 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)


if __name__ == '__main__':
    main()
