import errno
import xml.etree.ElementTree as ET
import os
import platform
import re
import sys
import subprocess
from itertools import islice

"""
Spacemacs shell git beautifier
Customize git output on Spacemacs shell

Author: Khoa Le
Version: Oct 19, 2018
"""

class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

def print_pair_info(commit, optional_commit):
    if len(optional_commit) == 0:
        prior_commit = subprocess.check_output(["git", "show", str(commit)+"^1"])
        prior_commit_msg = subprocess.check_output(["git", "log", "--format=%ci | %B", "-n 2", str(commit)+"^1"])

        current_commit = subprocess.check_output(["git", "show", str(commit)])
        current_commit_msg = subprocess.check_output(["git", "log", "--format=%ci | %B", "-n 1", str(commit)])
    else:
        prior_commit = subprocess.check_output(["git", "show", optional_commit])
        prior_commit_msg = subprocess.check_output(["git", "log", "--format=%ci | %B", "-n 2", optional_commit])

        current_commit = subprocess.check_output(["git", "show", str(commit)])
        current_commit_msg = subprocess.check_output(["git", "log", "--format=%ci | %B", "-n 1", str(commit)])

    cprint("\nSHOWING CHANGE BETWEEN:\n", bcolors.UNDERLINE)
    cprint(format_git(current_commit), bcolors.OKGREEN)
    cprint(format_git(current_commit_msg), bcolors.OKGREEN)
    print("-------------------------------------------------")
    cprint(format_git(prior_commit), bcolors.FAIL)
    cprint(format_git(prior_commit_msg) + "\n", bcolors.FAIL)

def print_command_output(output, color):
    lines = output.splitlines()
    i = 0
    for idx, line in enumerate(lines):
        cur_line = line.decode("utf-8")

        if len(cur_line) > 0:
            # sfc
            if "|\\" in cur_line or "|/" in cur_line:
                print(cur_line)
            elif "A" in cur_line[0]:
                cprint(cur_line, bcolors.OKGREEN)
            elif "D" in cur_line[0]:
                cprint(cur_line, bcolors.FAIL)
            elif "M" in cur_line[0]:
                cprint(cur_line, bcolors.OKBLUE)
            else:
                # other command options
                if "WARNING" in color:
                    cprint(cur_line, bcolors.WARNING)
                elif "OKBLUE" in color:
                    cprint(cur_line, bcolors.OKBLUE)
                elif "OKGREEN" in color:
                    cprint(cur_line, bcolors.OKGREEN)
                elif "BOLD" in color:
                    if idx % 2 == 0:
                        cprint("[" + str(i) +"]" + cur_line, bcolors.WARNING)
                        i+=1
                    else:
                        print("[" + str(i) +"]" + cur_line)
                        i+=1
                else:
                    if "On branch" in cur_line:
                        cprint(cur_line, bcolors.UNDERLINE)
                    elif "Changes not staged" in cur_line or "Untracked files" in cur_line:
                        cprint(cur_line, bcolors.WARNING)
                    elif "(use " not in cur_line:
                        print(cur_line)

def print_git_status_files(output):
    lines = output.splitlines()
    i = 1
    print("____________________________________________")
    for idx, line in enumerate(lines):
        cur_line = line.decode("utf-8")
        if "deleted: " in cur_line:
            cprint("[*]" + cur_line, bcolors.FAIL)
        if "modified:" in cur_line or "added:" in cur_line:
            cprint("[" + str(i) + "]" + cur_line, bcolors.OKBLUE)
            i+=1
        elif "new file:" in cur_line:
            cprint("[" + str(i) + "]" + cur_line, bcolors.OKGREEN)
            i+=1
        elif "Untracked files:" in cur_line:
            j = idx + 3
            line_contains_file = lines[j].decode("utf-8")
            while len(line_contains_file) > 0:
                print("[" + str(i) + "] Untracked files:" + line_contains_file)
                j+=1
                i+=1
                line_contains_file = lines[j].decode("utf-8")

def print_git_show_file_change(output):
    lines = output.splitlines()
    i = 1
    print("____________________________________________")
    for idx, line in enumerate(lines):
        cur_line = line.decode("utf-8")
        if "A" in cur_line[0]:
            cprint("[" + str(i) + "] " + cur_line, bcolors.OKGREEN)
            i+=1
        elif "D" in cur_line[0]:
            cprint("[X] " + cur_line, bcolors.FAIL)
        elif "M" in cur_line[0]:
            cprint("[" + str(i) + "] " + cur_line, bcolors.OKBLUE)
            i+=1

def print_git_branch(output):
    i = 1
    cprint("Git branch: ", bcolors.WARNING)
    for idx, line in enumerate(output):
        cprint("[" + str(i) + "] " + line, bcolors.OKGREEN)
        i+=1

def print_code_changed(output):
    lines = output.splitlines()
    file_name = ""
    for line in lines:
        cur_line = line.decode("utf-8")
        if "+++" in cur_line or "---" in cur_line:
                file_name = cur_line
        if "+" == cur_line[0]:
                cprint(cur_line[1:], bcolors.OKGREEN)
        elif "-" == cur_line[0]:
                cprint(cur_line[1:], bcolors.FAIL)
        elif "diff --git" in cur_line:
                cprint(cur_line, bcolors.WARNING)
        else:
                print(cur_line)
    cprint("\n Last printed: " + file_name[5: ], bcolors.WARNING)

def print_separation_line():
    left_fence = ""
    right_fence = ""
    dash_line = ""
    if "Windows" not in platform.system():
        for i in range (int(terminal_width()/3) - 8):
            left_fence += "_"
            right_fence += "_"
    else:
        for i in range (0,50):
            left_fence += "_"
            right_fence += "_"
    dash_line = left_fence + " ~ . ~ " + right_fence
    print(dash_line + "\n")

def print_help():
    print("SPACEMACS ESHELL GIT BEAUTIFIER")
    print("Commands")

def cprint(message, color):
    version = sys.version[0:1]
    if int(version) > 2:
        print("{0}{1}{2}".format(color,message,bcolors.ENDC))
    else:
        message = u' '.join((message, "")).encode('utf-8').strip()
        print("{0}{1}{2}".format(color,message,bcolors.ENDC))

'''
Return the first formatted output of the invoking git command
'''
def format_git(input):
    return input.splitlines()[0].decode("utf-8")

'''
Author: Alexander Belchenko
http://code.activestate.com/recipes/440694-determine-size-of-console-window-on-windows/
'''
def terminal_width():
    """Return estimated terminal width."""
    width = 0
    try:
        import struct, fcntl, termios
        s = struct.pack('HHHH', 0, 0, 0, 0)
        x = fcntl.ioctl(1, termios.TIOCGWINSZ, s)
        width = struct.unpack('HHHH', x)[1]
    except IOError:
        pass

    if width == 0:
        try:
            width = int(os.environ['COLUMNS'])
        except:
            pass
    if width == 0:
        width = 80

    return width
